# CarCar

Team:

* Kieran R - Services
* Colton S - Sales

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here:

Created a Domain Model file, just above this README.md.

I created models that represent the ability to create and book service appointments for automobiles.
These models are represented by the appointment itself, and the Technician who will preform the service.
Another Value Object model was created, based off of the Automobile models that are stored in the inventory microservice. I had to Create a poller to access the data from the inventory automobiles, and then use that data inside of my own Value Object model. With the correct data, I was then able to successfully display all of my necessary data to create an appointment. A Vin number, date, time, technician, and a reason, just to name a few pieces. Creating the React components were quite a challenge, at many times I became lost  and overwhelmed with where certain pieces of data were located and coming from. Our application has an extensive Nav bar, with many components like forms and lists of data. The inventory microservice gave us a solid blueprint on how to finish the rest of our own separate microservice. We completed the front end of the inventory within the first day and a half, and got started on services and sales right after. 

## Sales microservice

Explain your models and integration with the inventory
microservice, here.
I made models to provide the ability to create and list sales records for each salesperson.
